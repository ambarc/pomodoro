var express = require('express');
var app = express();
var url = require('url');

//app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.use(express.static(__dirname));

app.set('data', __dirname + '/data');
app.set('config', __dirname + '/config');

// Routes
app.get('/', function(request, response) {
  response.render('./index.html');
});

app.listen(process.env.PORT || 5000, function () {
  console.log('Example app listening on port 5000!');
})
