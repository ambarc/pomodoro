var PomodoroConstants = {

  ActionTypes: {
    START: 'start',
    STOP: 'stop',
    UPDATE_SETTINGS: 'update_settings',
    LOG_CYCLES: 'log_cycles',
    CLEAR_HISTORY: 'clear_history',
  },

  Events: {
    CHANGE: 'change',
  },

  Status: {
    RUNNING: 'running',
    STOPPED: 'stopped',
    COMPLETED: 'completed',
  },

  Durations: {
    DEFAULT_CYCLE_LENGTH_IN_MS: 6 * 1000,
    REFRESH_INTERVAL_IN_MS: 100,
    DEFAULT_SHORT_BREAK_IN_MS: 5  * 1000,
    DEFAULT_LONG_BREAK_IN_MS: 10 * 1000,
  },

  Counts: {
    DEFAULT_TARGET_COUNT: 7,
    DEFAULT_LONG_BREAK_FREQUENCY: 2,
  },

  Colors: {
    ACTIVE: 'red',
    REST: 'gray',
  },

  Dimensions: {
    BIG_CIRCLE_HEIGHT: 200,
    SMALL_CIRCLE_HEIGHT: 50,
  },

  CycleTypes: {
    ACTIVE: 'active',
    SHORT_BREAK: 'short_break',
    LONG_BREAK: 'long_break',
  }
};

module.exports = PomodoroConstants;
