var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var underscore = require('underscore');

var PomodoroDispatcher = require('../dispatcher/PomodoroDispatcher');
var PomodoroConstants = require('../constants/PomodoroConstants');

var _currentStatus = PomodoroConstants.Status.STOPPED;
var _historicalCycles = [];

/*
{
  duration, note, startTime, type,
}
*/
var CyclesStore = assign({}, EventEmitter.prototype, {
  start: function() {
    console.log('starting');
    if(_currentStatus !== PomodoroConstants.Status.RUNNING) {
      _currentStatus = PomodoroConstants.Status.RUNNING;
    }
  },

  stop: function () {
    console.log('stopping');
    if(_currentStatus !== PomodoroConstants.Status.STOPPED) {
      _currentStatus = PomodoroConstants.Status.STOPPED;
    }
  },

  getCurrentStatus: function() {
    return _currentStatus;
  },

  getHistoricalCycles: function() {
    return _historicalCycles;
  },

  getActiveCycles: function() {
    return underscore.filter(_historicalCycles, function(cycle) {
      return cycle.type === PomodoroConstants.CycleTypes.ACTIVE;
    });
  },

  clearHistoricalCycles: function() {
    _historicalCycles = [];
  },

  logCycles: function(cycles) {
    _historicalCycles = _historicalCycles.concat(cycles);
  },

  addChangeListener: function(callback) { 
    this.on(PomodoroConstants.Events.CHANGE, callback);
  },
});

CyclesStore.dispatchToken = PomodoroDispatcher.register(function(action) {
  switch(action.type) {
    case PomodoroConstants.ActionTypes.START:
      CyclesStore.start();
      CyclesStore.emit(PomodoroConstants.Events.CHANGE);
      break;
    case PomodoroConstants.ActionTypes.STOP:
      CyclesStore.stop();
      CyclesStore.emit(PomodoroConstants.Events.CHANGE);
      break;
    case PomodoroConstants.ActionTypes.LOG_CYCLES:
      CyclesStore.logCycles(action.cycles);
      CyclesStore.emit(PomodoroConstants.Events.CHANGE);
      break;
    case PomodoroConstants.ActionTypes.CLEAR_HISTORY:
      CyclesStore.stop();
      CyclesStore.clearHistoricalCycles();
      CyclesStore.emit(PomodoroConstants.Events.CHANGE);
      break;
    default:
      break;
  }
});

module.exports = CyclesStore;
