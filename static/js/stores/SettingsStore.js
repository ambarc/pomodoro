var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var PomodoroDispatcher = require('../dispatcher/PomodoroDispatcher');
var PomodoroConstants = require('../constants/PomodoroConstants');

var CyclesStore = require('./CyclesStore');

var _settings = {
  duration: PomodoroConstants.Durations.DEFAULT_CYCLE_LENGTH_IN_MS,
  totalCount: PomodoroConstants.Counts.DEFAULT_TARGET_COUNT,
  shortBreakLength: PomodoroConstants.Durations.DEFAULT_SHORT_BREAK_IN_MS,
  longBreakLength: PomodoroConstants.Durations.DEFAULT_LONG_BREAK_IN_MS,
  longBreakFrequency: PomodoroConstants.Counts.DEFAULT_LONG_BREAK_FREQUENCY
};
  
var SettingsStore = assign({}, EventEmitter.prototype, {

  // totalCount, duration, shortBreakLength, longBreakLength, longBreakFrequency
  updateSettings: function(settings) {
    console.log('updating settings to ', settings);
    _settings = settings;
  },

  getDuration: function() {
    return _settings.duration ?
      _settings.duration :
      PomodoroConstants.Durations.DEFAULT_CYCLE_LENGTH_IN_MS;
  },

  getShortBreakLength: function() {
    return _settings.shortBreakLength;
  },

  getLongBreakLength: function() {
    return _settings.longBreakLength;
  },

  getLongBreakFrequency: function() {
    return _settings.longBreakFrequency;
  },

  getTargetCycles: function() {
    return _settings.totalCount;
  },

  getSettings: function() {
    return _settings;
  },

  addChangeListener: function(callback) {
    this.on(PomodoroConstants.Events.CHANGE, callback);
  },
});

SettingsStore.dispatchToken = PomodoroDispatcher.register(function(action) {
  switch(action.type) {
    case PomodoroConstants.ActionTypes.UPDATE_SETTINGS:
      PomodoroDispatcher.waitFor([CyclesStore.dispatchToken]);
      SettingsStore.updateSettings(action.settings);
      SettingsStore.emit(PomodoroConstants.Events.CHANGE);
      break;
    default:
      break;
  }
});

module.exports = SettingsStore;
