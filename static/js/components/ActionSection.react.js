var React = require('react');

var PomodoroActionCreators = require('../actions/PomodoroActionCreators');
var PomodoroConstants = require('../constants/PomodoroConstants');

var ActionSection = React.createClass({
  render: function() {
    return (
      <div className="actionSection">
        <div className="actionButtons">
          {this.getActionButtons()}
        </div>
      </div>
    );
  },

  handleClick: function() {
    if (this.props.status === PomodoroConstants.Status.RUNNING) {
      PomodoroActionCreators.stop();
    } else {
      PomodoroActionCreators.start();
    }
  },

  getActionButtons: function() {
    var buttonText = "";
    switch(this.props.status) {
      case PomodoroConstants.Status.STOPPED:
        buttonText = 'Start';
        break;
      case PomodoroConstants.Status.RUNNING:
        buttonText = 'Stop';
        break;
      default:
        break;
    }
    return [(
      <button className="standardButton" onClick={this.handleClick}>
        {buttonText}
      </button>
    )];
  },
});

module.exports = ActionSection;
