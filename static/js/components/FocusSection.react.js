var React = require('react');

var PomodoroConstants = require('../constants/PomodoroConstants');
var BasicTimeSection = require('./timers/BasicTimeSection.react');
var CircleTimer = require('./timers/Circle.react');

var FocusSection = React.createClass({

  getMessage: function () {
    var toReturn = "";
    switch (this.props.cycleType) {
      case PomodoroConstants.CycleTypes.ACTIVE:
        toReturn = "Focus.";
        break;
      case PomodoroConstants.CycleTypes.SHORT_BREAK:
        toReturn = "Take a *short* break.";
        break;
      case PomodoroConstants.CycleTypes.LONG_BREAK:
        toReturn = "Freshen up. Take a long break.";
        break;
      default:
        break;
    }
    return toReturn;
  },
 
  render: function() {
    return (
      <div className="focusSection">
        <div className="focusTime">
          <BasicTimeSection
            duration={this.props.duration}
            remaining={this.props.remaining} />
          </div>
        <div className="focusText">
          {this.getMessage()}
        </div>
      </div>
    );
  },
});

module.exports = FocusSection;
