var React = require('react');

var PomodoroActionCreators = require('../actions/PomodoroActionCreators');

var SettingsSection = React.createClass({
  getInitialState: function() {
    return {};
  },

  render: function() {
    return (
      <div>
        Settings Pane
        <input value={this.state.totalCount}
          onChange={this.handleChange.bind(this, 'totalCount')}
          placeholder={'Enter the total number of cycles.'}
        /> 
        <input value={this.state.duration}
          onChange={this.handleChange.bind(this, 'duration')}
          placeholder={'Enter the duration of a cycle..'}
        />
        <input value={this.state.shortBreak}
          onChange={this.handleChange.bind(this, 'shortBreak')}
          placeholder={'Enter the duration of a short break.'}
        />
        <input value={this.state.longBreak}
          onChange={this.handleChange.bind(this, 'longBreak')}
          placeholder={'Enter the duration of a long break.'}
        />
        <input value={this.state.longBreakFrequency}
          onChange={this.handleChange.bind(this, 'longBreakFrequency')}
          placeholder={'Enter the frequency of a long break.'}
        />
        <button onClick={this.updateSettings}>
          Done
        </button>
      </div> 
    );
  },

  handleChange: function(key, event) {
    var toState = function() {
      var toReturn = {};
      toReturn[key] = parseInt(this.target.value);
      return toReturn;
    }.bind(event)();

    this.setState(toState);
  },

  updateSettings: function() {
    PomodoroActionCreators.updateSettings(this.state);
  },
});

module.exports = SettingsSection;
