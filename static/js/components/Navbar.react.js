var React = require('react');
var SettingsSection = require('./SettingsSection.react');

var Navbar = React.createClass({
  render: function() {
    return (
      <div className="navBar">
        <div className="navBarLeft">
        </div>
        <div className="navBarTitle">
          {this.props.title}
        </div>
        <div className="navBarSettings">
          <a href="#panel"><div className="settingsButton standardButton">Settings</div></a>
        </div>
        <a id="panel" href="#"><SettingsSection /></a>
      </div>
    );
  },
});

module.exports = Navbar;
