var PomodoroConstants = require('../../constants/PomodoroConstants');
var React = require('react');
var moment = require('moment');

var TimerMixin = {

  propTypes: {
    duration: React.PropTypes.number.isRequired,
  },

  getDefaultProps: function() {
    return {
      duration: PomodoroConstants.Durations.DEFAULT_CYCLE_LENGTH_IN_MS,
    };
  },

  getInitialState: function() {
    return {
      elapsed: 0,
      status: PomodoroConstants.Status.RUNNING,
      remaining: this.props.duration,
      minutes: this.getMinutes(this.props.duration),
      seconds: this.getSeconds(this.props.duration),
      tickCount: 0,
    };
  },

  componentDidMount: function() {
    this.tick();
  },

  componentWillUnmount: function() {
    if (this.state.interval) {
      this.stopTicking(); 
    }
  },

  componentDidUpdate: function(prevProps, prevState) {
    if (prevState.status !== PomodoroConstants.Status.RUNNING &&
        this.state.status === PomodoroConstants.Status.RUNNING) {
      this.tick();
    }
  },

  pause: function() {
    this.stopTicking();
    this.setState({
      status: PomodoroConstants.Status.STOPPED,
      lastMeasured: moment().unix(),
    });
  },

  reset: function() {
    this.stopTicking();
    this.setState({
      status: PomodoroConstants.Status.STOPPED,
      elapsed: 0,
      lastMeasured: moment().unix(),
    });
  },

  // start or resume.
  go: function() {
    this.setState({
      status: PomodoroConstants.Status.RUNNING,
      lastMeasured: moment().unix(),
    });
  },

  stopTicking: function () {
    clearTimeout(this.state.interval);
  },

  tick: function() {
    if (this.state.status === PomodoroConstants.Status.RUNNING) {
      var now = moment().valueOf();
      var change;
      if (this.state.lastMeasured) {
        change = this.state.lastMeasured;
      } else {
        change = now;
      }
      var elapsed = this.state.elapsed + (now - change);
      var remaining = this.props.duration - this.state.elapsed;
      //console.log('tick ',this.state.tickCount, remaining);

      if (remaining > 0) {

        this.setState({
          lastMeasured: now,
          elapsed: elapsed,
          remaining: remaining,

          minutes: this.getMinutes(remaining),
          seconds: this.getSeconds(remaining),
          tickCount: this.state.tickCount + 1,

          interval: setTimeout(this.tick, PomodoroConstants.Durations.REFRESH_INTERVAL_IN_MS),
        });
      } else if (this.state.interval) {
        this.stopTicking();
      }
    }
  },

  getMinutes: function(ms) {
    return Math.floor(ms / (60 * 1000));
  },

  getSeconds: function (ms) {
    return Math.floor((ms / 1000) % 60);
  },

};

module.exports = TimerMixin;
