var React = require('react');
var moment = require('moment');

var PomodoroConstants = require('../../constants/PomodoroConstants');

var TimeUtils = require('../../utils/TimeUtils');

// TODO - better name related to progress..
var Circle = React.createClass({

  propTypes: {
    dimension: React.PropTypes.number,
    duration: React.PropTypes.number,
    remaining: React.PropTypes.number,
  },

  getTimeDisplay: function() {
    var ratio = (this.props.remaining/this.props.duration);
    var circleStyle = {
      'width': this.props.dimension + 'px',
      'height': this.props.dimension + 'px',
      'backgroundColor': 'black', // TODO clean
    };

    var fillStyle = {
      'width': (this.props.dimension * 1.3) + 'px',
      'height': (ratio * this.props.dimension) + 'px',
      'backgroundColor': this.props.isActive ? PomodoroConstants.Colors.ACTIVE : PomodoroConstants.Colors.REST,
    }
  
    var toReturn = (
      <div className="circleContainer">

          <div style={circleStyle} className="timerCircle">

        <div className="timerCircleFill" style={fillStyle}>
        </div>
          </div>
      </div>
    );
    return toReturn;
  },

  render: function() {
    return (
      <div>
        <div className="circleTimeDisplay">
          {this.getTimeDisplay()}
        </div>
      </div>
    );
  },
});

module.exports = Circle;
