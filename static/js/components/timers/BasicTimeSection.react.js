var React = require('react');
var moment = require('moment');

var TimeUtils = require('../../utils/TimeUtils');

var BasicTimeSection = React.createClass({

  getTimeDisplay: function() {
    var toReturn = null;

    var minutes = TimeUtils.getMinutes(this.props.remaining);
    var seconds = TimeUtils.getSeconds(this.props.remaining);

    if (this.props.remaining > 0) {
      toReturn = "" + this.pad(minutes) + ":" + this.pad(seconds);
    } else {
      toReturn = "Finished";
    }
    return toReturn;
  },

  pad: function(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
  },

  render: function() {
    return (
      <div>
        <div className="basicTimeDisplay">
          {this.getTimeDisplay()}
        </div>
      </div>
    );
  },
});

module.exports = BasicTimeSection;
