var React = require('react');
var moment = require('moment');
var underscore = require('underscore');

var PomodoroConstants = require('../constants/PomodoroConstants');
var PomodoroActionCreators = require('../actions/PomodoroActionCreators');

var ActionSection = require('./ActionSection.react');
var FocusSection = require('./FocusSection.react');
var HistorySection = require('./HistorySection.react');
var Navbar = require('./Navbar.react');
var SettingsSection = require('./SettingsSection.react');

var CyclesStore = require('../stores/CyclesStore');
var SettingsStore = require('../stores/SettingsStore');

var TimeUtils = require('../utils/TimeUtils');

var PomodoroApp = React.createClass({

  componentDidMount: function() {
    CyclesStore.addChangeListener(this.handleCyclesChange);
    SettingsStore.addChangeListener(this.handleSettingsChange);
  },

  componentWillUnmount: function() {
    CyclesStore.removeChangeListener(this.handleCyclesChange);
    SettingsStore.removeChangeListener(this.handleSettingsChange);
  },

  getInitialState: function() {
    return underscore.extend(
      {},
      this.getResetInfo()
    );
  },

  componentDidUpdate: function(oldProps, oldState) {
    if (oldState.status === PomodoroConstants.Status.RUNNING &&
        this.state.status !== PomodoroConstants.Status.RUNNING) {
      this.stopTicking();
    } else if (this.state.status === PomodoroConstants.Status.RUNNING &&
               this.state.lastMeasured === null) {
      this.tick(); // todo - clean
    } else if (oldState.status === PomodoroConstants.Status.STOPPED &&
               this.state.status !== PomodoroConstants.Status.STOPPED) {
      this.tick();
    }
  },

  handleCyclesChange: function() {
    if (CyclesStore.getActiveCycles().length >= SettingsStore.getTargetCycles()) {
      this.setState(this.getResetInfo());
    } else {
      var toSet = {};
      if (this.state.elapsed >= this.state.duration) {
        underscore.extend(toSet, this.getNextCycleInfo());
      }
      this.setState(underscore.extend(toSet, {
        status: CyclesStore.getCurrentStatus(),
        historicalCycles: CyclesStore.getHistoricalCycles(),
      }));
    }
  },

  handleSettingsChange: function() {
    this.setState(this.getResetInfo());
  },

  stopTicking: function () {
    if (this.state.interval) {
      clearTimeout(this.state.interval);
    }
    if (CyclesStore.getCurrentStatus() === PomodoroConstants.Status.STOPPED) {
      this.setState({
        lastMeasured: null,
      });
    }
  },

  tick: function() {
    if (this.state.status === PomodoroConstants.Status.RUNNING) {
      var now = moment().valueOf();
      var mark;
      if (this.state.lastMeasured) {
        mark = this.state.lastMeasured;
      } else {
        mark = now;
      }
      var elapsed = this.state.elapsed + (now - mark);
      var remaining = this.state.duration - this.state.elapsed;
      //console.log('tick ', this.state.tickCount, remaining);

      if (remaining > 0) {

        this.setState({
          lastMeasured: now,
          elapsed: elapsed,
          remaining: remaining,

          minutes: TimeUtils.getMinutes(remaining),
          seconds: TimeUtils.getSeconds(remaining),
          tickCount: this.state.tickCount + 1,

          interval: setTimeout(this.tick, PomodoroConstants.Durations.REFRESH_INTERVAL_IN_MS),
        });
      } else {
        this.finishCycleAndProceed();
      }
    }
  },

  finishCycleAndProceed: function() {
    // Log a completion
    this.stopTicking();
    PomodoroActionCreators.logCycles([this.getCurrentCycleInfo()]);
  },

  completeSequence: function() {
  },

  getNextCycleInfo: function() {
    var targetCycles = SettingsStore.getTargetCycles();
    var historicalCycles = CyclesStore.getHistoricalCycles();
    var activeCycles = CyclesStore.getActiveCycles();
    var toReturn = {};
    if (activeCycles.length < targetCycles) {
      var useDuration = SettingsStore.getDuration();
      var nextCycleType = PomodoroConstants.CycleTypes.ACTIVE;
      var nextCycleLength = SettingsStore.getDuration();
      
      if (historicalCycles.length > 0) {
        var lastCycle = historicalCycles[historicalCycles.length - 1];
        if (lastCycle.type === PomodoroConstants.CycleTypes.ACTIVE) {
          if (activeCycles.length % SettingsStore.getLongBreakFrequency() === 0) {
            nextCycleType = PomodoroConstants.CycleTypes.LONG_BREAK;
            nextCycleLength = SettingsStore.getLongBreakLength();
          } else {
            nextCycleType = PomodoroConstants.CycleTypes.SHORT_BREAK;
            nextCycleLength = SettingsStore.getShortBreakLength();
          }
        }
      }
      toReturn = {
        cycleType: nextCycleType,
        duration: nextCycleLength,
        lastMeasured: null,
        elapsed: 0,
      };
    }
    return toReturn;
  },

  getCurrentCycleInfo: function() {
    return {
      type: this.state.cycleType,
      duration: this.state.duration,
    }
  },

  getActivitySectionStyle: function() {
    var backgroundColor = PomodoroConstants.Colors.ACTIVE;
    if (this.getCurrentCycleInfo().type !== PomodoroConstants.CycleTypes.ACTIVE) {
      backgroundColor = PomodoroConstants.Colors.REST;
    }
    return {
      backgroundColor: backgroundColor,
    }
  },

  render: function() {
    return (
      <div>
        <Navbar title="A Pomodoro App"/>
        <div className="activitySection" style={this.getActivitySectionStyle()}>
          <FocusSection
            cycleType={this.getCurrentCycleInfo().type}
            duration={this.state.duration}
            remaining={this.state.remaining} />
          <ActionSection status={CyclesStore.getCurrentStatus()} />
          <HistorySection
            totalCycles={SettingsStore.getTargetCycles()}
            historicalActiveCycles={CyclesStore.getActiveCycles()}
            currentType={this.getCurrentCycleInfo().type}
            activeDuration={SettingsStore.getDuration()}
            activeRemaining={this.state.remaining} />
        </div>
      </div>
    );
  },

  getResetInfo: function() {
    // TODO - could use some cleaning/demeter.
    var nextCycleInfo = this.getNextCycleInfo();
    var useDuration = SettingsStore.getDuration();
    return {
      status: CyclesStore.getCurrentStatus(),
      duration: nextCycleInfo.duration,
      // Active cycle.
      lastMeasured: nextCycleInfo.lastMeasured,
      elapsed: nextCycleInfo.elapsed,
      remaining: nextCycleInfo.duration,
      tickCount: 0,
      cycleType: PomodoroConstants.CycleTypes.ACTIVE,
    };
  },
});

module.exports = PomodoroApp;
