var React = require('react');
var underscore = require('underscore');

var PomodoroConstants = require('../constants/PomodoroConstants');

var BasicTimeSection = require('./timers/BasicTimeSection.react');
var CircleTimer = require('./timers/Circle.react');

var dimension = 50;
var HistorySection = React.createClass({
  propTypes: {
    historicalActiveCycles: React.PropTypes.array,
    activeDuration: React.PropTypes.number,
    activeRemaining: React.PropTypes.number,
    totalCycles: React.PropTypes.number,
    currentType: React.PropTypes.string,
  },

  getCycleElements: function() {
    var toReturn = [];
    var isActive = this.props.currentType === PomodoroConstants.CycleTypes.ACTIVE;

    for (cycle in this.props.historicalActiveCycles) {
      toReturn.push(this.getCycleElement(cycle, this.props.activeDuration, 0, isActive));
    }

    if (isActive) {
      toReturn.push(this.getCycleElement(toReturn.length,
                                         this.props.activeDuration,
                                         this.props.activeRemaining,
                                         isActive));
    }

    var cyclesLeft = this.props.totalCycles === this.props.historicalActiveCycles.length ?
      0 : this.props.totalCycles - (this.props.historicalActiveCycles.length);
    if (isActive) {
      cyclesLeft -= 1;
    }
    for (i = 0; i < cyclesLeft; i++) {
      toReturn.push(this.getCycleElement(i + toReturn.length,
                                         this.props.activeDuration,
                                         this.props.activeDuration,
                                         isActive)); 
    }
    return (<div>{toReturn}</div>);
  },

  getCycleElement: function(index, duration, remaining, isActive) {
    return (
      <div className="historyEntry">
        <CircleTimer
        style={this.getCycleStyle(index)}
        dimension={PomodoroConstants.Dimensions.SMALL_CIRCLE_HEIGHT}
        duration={duration}
        isActive={isActive}
        remaining={remaining} />
      </div>
    );
  },
  
  getCycleStyle: function(index) {
    return {
      order: index,
    };
  },

  render: function() {
    return (
      <div className="historyContainer">
        History
        {this.getCycleElements()}
      </div>
    );
  },
});

module.exports = HistorySection;
