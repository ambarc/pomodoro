var TimeUtils = {
  getMinutes: function(ms) {
    return Math.floor(ms / (60 * 1000));
  },

  getSeconds: function (ms) {
    return Math.floor((ms / 1000) % 60);
  },
};

module.exports = TimeUtils;
