var React = require('react');
var ReactDOM = require('react-dom');

var PomodoroApp = require('./components/PomodoroApp.react');
window.React = React;

ReactDOM.render(<PomodoroApp />, document.getElementById('mainSection'));
