var PomodoroDispatcher = require('../dispatcher/PomodoroDispatcher');
var PomodoroConstants = require('../constants/PomodoroConstants');

var PomodoroActionCreators = {

  start: function() {
    PomodoroDispatcher.dispatch({
      type: PomodoroConstants.ActionTypes.START,
    });
  },

  stop: function() {
    PomodoroDispatcher.dispatch({
      type: PomodoroConstants.ActionTypes.STOP,
    });
  },

  clearHistory: function() {
    PomodoroDispatcher.dispatch({
      type: PomodoroConstants.ActionTypes.CLEAR_HISTORY,
    });
  },

  updateSettings: function(settings) { 

    this.clearHistory();

    PomodoroDispatcher.dispatch({
      type: PomodoroConstants.ActionTypes.UPDATE_SETTINGS,
      settings: settings,
    });
  },

  logCycles: function(cycles) {
    PomodoroDispatcher.dispatch({
      type: PomodoroConstants.ActionTypes.LOG_CYCLES,
      cycles: cycles,
    });
  },

};

module.exports = PomodoroActionCreators;
